[Gatomontez Theme](https://bitbucket.org/oarodriguez/gatomontez-theme)
-----------------------------------------------------------------------

Base theme for [Gatomontez.com](http://gatomontez.com) website.


## Credits

Gatomontez Theme is based on [Roots Starter Theme](http://roots.io/).
