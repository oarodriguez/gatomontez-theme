<footer class="content" role="contentinfo">
  <div class="container">
    <div class="row">
      <div class="footer-left">
        <?php dynamic_sidebar('sidebar-footer-left'); ?>
      </div>
      <div class="footer-middle">
        <?php dynamic_sidebar('sidebar-footer-middle'); ?>
      </div>
      <div class="footer-right">
        <?php dynamic_sidebar('sidebar-footer-right'); ?>
      </div>
    </div>
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
